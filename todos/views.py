from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from .forms import TodoListForm


# Create your views here.
def todo_list_update_view(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_deatail', id=todo_list.id)
    form = TodoListForm(instance=todo_list)
    context = {'form': form, 'todo_list': todo_list}
    return render(request, 'todo/todo_list_update.html', context)

def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists,
    }
    return render(request, 'todos/todo_list.html', context)

def todo_list_detail_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list})

def todo_list_create_view(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_create.html', {'form': form})

def todo_list_delete_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        todo_list.delete()
        return redirect('todos:todo_list_list')

    context = {'todo_list': todo_list}
    return render(request, 'todos/todo_list_delete.html', context)

def todo_item_create_view(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(commit=False)
            todo_item.list = TodoList.objects.get(id=request.POST['list'])
            todo_item.save()  # Save the TodoItem
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        'form': form,
    }
    return render(request, 'todos/todo_item_create.html', context)

def todo_item_update_view(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=item)
    return render(request, 'todos/todo_item_update.html', {'form': form, 'item': item})
