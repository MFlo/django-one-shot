from django.urls import path
from . import views

app_name = 'todos'

urlpatterns = [
    # Feature 7: List view for TodoList
    path('', views.todo_list_view, name='todo_list_list'),

    # Feature 8: Detail view for a specific TodoList
    path('<int:id>/', views.todo_list_detail_view, name='todo_list_detail'),

    # Feature 9: Create view for a new TodoList
    path('create/', views.todo_list_create_view, name='todo_list_create'),

    # Feature 10: Update view for an existing TodoList
    path('<int:id>/edit/', views.todo_list_update_view, name='todo_list_update'),

    # Feature 11: Delete view for an existing TodoList
    path('<int:id>/delete/', views.todo_list_delete_view, name='todo_list_delete'),

    # Feature 12: Create view for a to-do list item
    path('items/create/', views.todo_item_create_view, name='todo_item_create'),

    # Feature 13: Create view for item update
    path('items/<int:id>/edit/', views.todo_item_update_view, name='todo_item_update'),
]
